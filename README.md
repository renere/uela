# Uela 
An icon theme based on Tela and Fluent. (https://github.com/vinceliuice/Tela-icon-theme https://github.com/vinceliuice/Fluent-icon-theme)

This icon theme continues the flat-ness and modernity of the Tela icon theme whilst introducing more minimalist designs, rounded corners, and softer colours.

This is currently very much a work in progress, so expect some icons to not be changed from Tela right now.

